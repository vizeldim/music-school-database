# Semestralni prace - Databazove systemy
## Zadani
- Úvodní odborný článek s popisem domény, který slouží jako podklad pro návrh struktury datového úložiště. Součástí je alespoň 25 rozmanitých dotazů formulovaných v přirozeném jazyku. Tyto dotazy musí, mimo další kategorie, pokrývat povinně kategorie A, B, C, D atd. z Tabulky kategorií dotazů.

- Návrh struktury datového úložiště ve formě konceptuálního schématu, které odpovídá popisu domény.

- Implementace ve zvoleném databázovém stroji (k dispozici máte databázi Oracle, implementovat však můžete v libovolném stroji - to však nedoporučujeme v souběhu s portálem DBS)

- SQL skript pro naplnění databáze vhodnými testovacími daty v dostatečném množství

- Alespoň 10 dotazů zmíněných v popisu formulovaných v relační algebře

- Alespoň 25 dotazů v jazyce SQL nad vaším úložištěm, mezi nimiž bude deset dotazů formulovaných výše v relační algebře a také příkazy pro manipulaci s daty

## Popis domeny - Soukromá hudební škola
Soukromá hudební škola v Praze si potřebuje vytvořit databázi dle následujicího popisu:

Ve škole pracují Zaměstnanci. Zaznamenáváme jejích jméno, příjmení, kontaktní údaje ( email, telefon ), číslo účtu a plat. Každý zaměstnanec musí pracovat alespoň na jedné pozici (učitel, ředitel, učetní, uklízeč..). Na určité pozici muže pracovat více zaměstnanců. Zaměstnanec bydlí na určite adrese (=město, PSČ, ulice, číslo popisné a orientační).

Škola má 3 obory (teoretický, instrumentální a pěvecký). U oboru zaznamenáváme název. V rámci jednotlivých oborů se vyučují předměty,které probíhají ve formě individuálních, nebo skupinových lekcí. U jednotlivých lekcí předmětů zapisujeme poznámku, kde učitel lekce daného předmětu může zaznamenat například.: konkrétní program, dle kterého vyučuje, nebo jiné preference. Někteří učitele nechtěji (v případě individuálních lekcí) vyučovat úplné začátečníky, proto se u individuální lekce určuje, zda je vhodná pro začátečníky či nikoliv. U skupinové lekce může učitel určit doporučenou kapacitu dané lekce a nějak danou skupinu pojmenovat. Učitel může mít více lekcí a konkrétní lekce musí být přířazená právě jednomu učiteli.

Do školy chodí žáci. Každý z nich má právo navštěvovat jednu nebo více různých lekcí. Musí být starší 5 let. Je třeba zaznamenat jeho jméno, příjmeni, kontaktní údaje ( email, telefon ), datum narození. Každý žák bydlí na určité adrese. Dále je zcela nezbytné evidovat jméno, příjmeni a kontaktní údaje ( email, telefon ) zákonného zástupce těch žáků, kteří nedosáhli 18 let.

Žák se může účastnit koncertů. U koncertu potřebujeme zaznamenat název, popis,datum a místo konání (nemusí nutně probíhat v sále školy).

Dále škola vlastní hudební nástroje a poskytuje možnost si nástroje (určené k půjčení) půjčit žákům. U hudebních nástrojů evidujeme název, výrobce, datum zakoupeni, cenu a je-li určen k půjčeni. Požadavkem je zaznamenávat pouze aktuální půjčku konkrétního nástroje a datum jejího uskutečněni. Žák může mít více půjček, ale půjčka se vztahuje na konkrétní nástroj. Dále evidujeme jejích výrobce a prodejce za účelem případné budoucí reklamace. U prodejce zaznamenáváme (unikátní) název,email, telefon nebo web. Každý konkrétní nástroj spadá do určité kategorie nástrojů (housle, flétny, hoboje...). U kategorie si vystačíme s názvem, který musí být unikátní.

## Konceptualni schema
![Konceptualni schema](/references/conceptual_schema.png)

## Relacni schema
![Relacni schema](/references/relational_schema.png)

## SQL dotazy

### Seznam všech zaměstnanců, kteří bydlí mimo Prahu
#### RA
```
adresa(mesto!='Praha') *> zamestnanec
```
#### SQL
```
SELECT z.*
FROM zamestnanec z
JOIN adresa a ON z.id_adresa=a.id_adresa
WHERE mesto!='Praha';
```


### Vyber žáky(id, jmeno), kteři mají půjčený nástroj(alespoň jeden).
#### RA
```
{pujcka *> zak}[id_zak, jmeno]
```
#### SQL
```
SELECT DISTINCT z.id_zak,
                z.jmeno
FROM zak z
JOIN pujcka p ON z.id_zak=p.id_zak;
```


### Seznam nástrojů(id, nazev = model, cena) zakoupených u prodejce, jehož web je 'www.muziker.cz'. Ke každému vypiš výrobce a kategorii, do které patři.
#### RA
```
{{prodejce(web='www.muziker.cz')[prodejce.id_prodejce=hudebni_nastroj.id_prodejce]hudebni_nastroj[hudebni_nastroj.id_kategorie=kategorie.id_kategorie]kategorie}[hudebni_nastroj.id_vyrobce=vyrobce.id_vyrobce]vyrobce}
[kategorie.nazev -> kategorie,
 vyrobce.nazev -> vyrobce,
 id_hudebni_nastroj,
 hudebni_nastroj.nazev -> model,
 prodejce.nazev -> prodejce,
 hudebni_nastroj.cena]
```
#### SQL
```
SELECT k.nazev AS kategorie,
       v.nazev AS vyrobce,
       id_hudebni_nastroj,
       h.nazev AS model,
       p.nazev AS prodejce,
       h.cena
FROM hudebni_nastroj h 
JOIN prodejce p ON h.id_prodejce=p.id_prodejce
JOIN kategorie k ON k.id_kategorie=h.id_kategorie
JOIN vyrobce v ON v.id_vyrobce=h.id_vyrobce
WHERE web='www.muziker.cz';
```

### Jména všech učitelů předmětu 'Hra na flétnu'.
#### RA
```
{pozice(nazev='učitel') *> zamestnanec_pozice_rel *> zamestnanec * lekce * predmet(nazev='Hra na flétnu')}[id_zamestnanec, jmeno]
```
#### SQL
```
SELECT DISTINCT id_zamestnanec,
                jmeno 
FROM zamestnanec
JOIN zamestnanec_pozice_rel using(id_zamestnanec)
JOIN pozice po using(id_pozice)
JOIN lekce using(id_zamestnanec) 
JOIN predmet pr using(id_predmet)
WHERE po.nazev='učitel' AND pr.nazev = 'Hra na flétnu';
```


### Seznam nástrojů spadajicích do kategorie 'Klavír' od výrobce 'Yamaha'
#### RA
```
{kategorie(nazev='Klavír')[kategorie.id_kategorie = hudebni_nastroj.id_kategorie>hudebni_nastroj<hudebni_nastroj.id_vyrobce = vyrobce.id_vyrobce]vyrobce(nazev='Yamaha')}
[id_hudebni_nastroj, nazev, cena, datum_zakoupeni]
```
#### SQL
```
SELECT id_hudebni_nastroj,
       h.nazev,
       h.cena,
       h.datum_zakoupeni
FROM hudebni_nastroj h
JOIN kategorie k USING(id_kategorie)
JOIN vyrobce v USING(id_vyrobce)
WHERE v.nazev='Yamaha' AND k.nazev='Klavír';
```


### Všechny koncerty, které proběhly v roce 2018, jejíchž účastníkem byl žák jménem 'Tomáš Černý'
#### RA
```
zak(jmeno='Tomáš Černý') *> koncert_zak_rel *> koncert(datum>='01.01.2018' ∧ datum<='31.12.2018')
```
#### SQL
```
SELECT k.* 
FROM koncert k
JOIN koncert_zak_rel kzr ON k.id_koncert=kzr.id_koncert
JOIN zak using(id_zak)
WHERE TO_CHAR(datum, 'YYYY')=2018 and jmeno='Tomáš Černý';
```


### Všechny koncerty, které proběhly v roce 2018, jejíchž účastníkem byl žák jménem 'Tomáš Černý'
#### RA
```
{koncert(datum='26.02.2019') *> koncert_zak_rel *> zak}[id_zak, jmeno]
```
#### SQL
```
SELECT DISTINCT id_zak,
                jmeno
FROM 
(
    SELECT id_koncert
    FROM koncert
    WHERE datum = TO_DATE('26.02.2019','dd.mm.yyyy')
) 
JOIN koncert_zak_rel using(id_koncert)
JOIN zak using(id_zak);
```



### Najdi všechny zaměstnance, kteří pracují POUZE na pozici 'učitel'
#### RA
```
{pozice(nazev='učitel') *> zamestnanec_pozice_rel *> zamestnanec}
\
{pozice(nazev!='učitel') *> zamestnanec_pozice_rel *> zamestnanec}
```
#### SQL
```
(
SELECT z.*
FROM zamestnanec z
JOIN zamestnanec_pozice_rel zpr ON z.id_zamestnanec = zpr.id_zamestnanec
JOIN pozice p ON p.id_pozice = zpr.id_pozice
WHERE p.nazev = 'učitel'
)

MINUS

(
SELECT z.*
FROM zamestnanec z
JOIN zamestnanec_pozice_rel zpr ON z.id_zamestnanec = zpr.id_zamestnanec
JOIN pozice p ON p.id_pozice = zpr.id_pozice
WHERE p.nazev != 'učitel'
);
```


### Jména žáků, kteří se zúčastnili VŠECH koncertů v roce 2017
#### RA
```
{{koncert_zak_rel[id_zak, id_koncert] ÷ koncert(datum<='31.12.2017' ∧ datum>='01.01.2017')[id_koncert]}*zak}[id_zak, jmeno]
```
#### SQL
```
--Vyber zaky, pro nez neexistuje takovy koncert v roce 2017, ktereho by se nezucastnili
SELECT id_zak,
       jmeno
FROM zak z
WHERE NOT EXISTS
(
    SELECT *
    FROM koncert k
    WHERE TO_CHAR(datum, 'YYYY')=2017 AND NOT EXISTS
    (
        SELECT * FROM koncert_zak_rel kzr WHERE kzr.id_zak=z.id_zak AND kzr.id_koncert=k.id_koncert
    )
);
```



### Ověření dotazu číslo 12 - Seznam všech koncertů v roce 2017 mínus seznam koncertů v roce 2017, kterých se zúčastnili žáci, kteří jsou výstupem dotazu 12. => musí být prázdná množina

#### RA
```
{koncert(datum>='01.01.2017' ∧ datum <= '31.12.2017')}
\
{{koncert_zak_rel[id_zak, id_koncert] ÷ koncert(datum<='31.12.2017' ∧ datum>='01.01.2017')[id_koncert]}*>koncert_zak_rel*>koncert}
```
#### SQL
```
SELECT *
FROM koncert
WHERE TO_CHAR(datum, 'YYYY')=2017 AND id_koncert NOT IN
(
    SELECT id_koncert
    FROM koncert_zak_rel
    WHERE id_zak IN
    (
        SELECT id_zak FROM zak z WHERE NOT EXISTS
        (
            SELECT *
            FROM koncert k
            WHERE TO_CHAR(datum, 'YYYY')=2017 AND NOT EXISTS
            (
                SELECT *
                FROM koncert_zak_rel kzr
                WHERE kzr.id_zak=z.id_zak AND kzr.id_koncert=k.id_koncert
            )
        )
    )
);
```

### Žáci(id_zak, jmeno), kteří navšěvují lekce předmětu 'Hra na harfu' nebo 'Hra na saxofon'.

#### RA
```
{predmet(nazev='Hra na harfu' ∨  nazev='Hra na saxofon') *> lekce *> zak_lekce_rel *> zak}[id_zak,jmeno]
```

#### SQL
```
(
    SELECT DISTINCT id_zak,
                    jmeno
    FROM zak
    JOIN zak_lekce_rel using(id_zak)
    JOIN lekce using(id_lekce)
    JOIN predmet p using(id_predmet)
    WHERE p.nazev='Hra na harfu'
)

UNION

(
    SELECT DISTINCT id_zak,
                    jmeno
    FROM zak
    JOIN zak_lekce_rel using(id_zak)
    JOIN lekce using(id_lekce)
    JOIN predmet p using(id_predmet)
    WHERE p.nazev='Hra na saxofon'
);
```

### Žáci (id_zak, jmeno), kteří se zúčastnili koncertů s názvem 'Vánoční koncert' dne '08.12.2018' a zároveň se zúčastnili koncertů s názvem 'Vánoční koncert' dne '12.12.2019'.

#### RA
```
{koncert(datum='12.12.2019' and nazev='Vánoční koncert') *> koncert_zak_rel *> zak}[id_zak, jmeno]
∩
{koncert(datum='08.12.2018' and nazev='Vánoční koncert') *> koncert_zak_rel *> zak}[id_zak, jmeno]
```

#### SQL
```
(
    SELECT DISTINCT id_zak,
                    jmeno
    FROM zak
    JOIN koncert_zak_rel using(id_zak)
    JOIN koncert using(id_koncert)
    WHERE datum='12.12.2019' AND nazev='Vánoční koncert'
)

INTERSECT

(
    SELECT DISTINCT id_zak,
                    jmeno
    FROM zak
    JOIN koncert_zak_rel using(id_zak)
    JOIN koncert using(id_koncert)
    WHERE datum='08.12.2018' AND nazev='Vánoční koncert'
);
```



### Žáci, kteří se nikdy nezúčastnili žádného koncertu

#### RA
```
zak !<* koncert_zak_rel
```

#### SQL
```
SELECT *
FROM zak z
WHERE NOT EXISTS
(
    SELECT *
    FROM koncert_zak_rel k
    WHERE k.id_zak=z.id_zak
);
```



### D13 (Žáci, kteří se nikdy nezúčastnili žádného koncertu) - tři formulace v SQL

#### SQL
```
--FORMULACE 1
SELECT *
FROM zak z
WHERE NOT EXISTS
(
    SELECT *
    FROM koncert_zak_rel k
    WHERE k.id_zak=z.id_zak
);


--FORMULACE 2
(SELECT * FROM zak)
MINUS
(SELECT z.* FROM zak z JOIN koncert_zak_rel k ON z.id_zak=k.id_zak );


--FORMULACE 3
SELECT * FROM zak WHERE id_zak NOT IN( SELECT id_zak FROM koncert_zak_rel);
```


### D15 - Maximální cena hudebního nástroje.
#### SQL
```
SELECT MAX(cena) AS maximalni_cena
FROM hudebni_nastroj;
```



### D16 - Počet žáků, kteří chodí na lekce předmětu 'Hra na klavír'
#### SQL
```
SELECT count(distinct id_zak) as pocet_zaku
FROM zak
JOIN zak_lekce_rel using(id_zak)
JOIN lekce using(id_lekce)
JOIN predmet p using(id_predmet)
WHERE p.nazev='Hra na klavír';
```

### D17 - Kolik by bylo celkem zápisů na lekce, kdyby byl každý žák zapsán na každou lekci právě jednou?

#### SQL
```
SELECT count(*) AS pocet_zapisu
FROM lekce
CROSS JOIN zak;
```


### D18 - Pro každý koncert (id_koncert, nazev, datum) vypiš datum narozeni nejmladšího žáka

#### SQL
```
--vnoreny dotaz
SELECT id_koncert,
       k.nazev,
       k.datum,
      (SELECT max(datum_narozeni)
       FROM zak z
       NATURAL JOIN koncert_zak_rel kzr
       WHERE k.id_koncert=kzr.id_koncert) as datum_narozeni
FROM  koncert k
WHERE id_koncert IN
(
    SELECT id_koncert
    FROM koncert_zak_rel
)
order by id_koncert asc;

--group by
SELECT id_koncert,
       nazev,
       datum,
       max(datum_narozeni) as datum_narozeni
FROM koncert k
JOIN koncert_zak_rel using(id_koncert)
JOIN zak using(id_zak)
GROUP BY id_koncert,
         nazev,
         datum
ORDER BY id_koncert ASC;
```


### D19 - Počet účastníků jednotlivých koncertů (které se konaly -> byly na ně zapsaní žáci), seřazených dle data konáni od nejstaršího. (id koncertu, datum, název , počet účastníků)

#### SQL
```
SELECT id_koncert,
       datum,
       nazev,
       count(id_zak) as pocet_ucastniku
FROM koncert
JOIN koncert_zak_rel using(id_koncert)
JOIN zak using(id_zak)
GROUP BY id_koncert,
         nazev,
         datum
ORDER BY datum ASC;
```


### D20 - Součet cen a počet všech nástrojů zakoupených v roce 2019 v rámci jednotlivých kategorii. (Seřazené podle celkové ceny sestupně). Vypiš pouze ty kategorie, u níchž je celková cena vyšší než 200000.

#### SQL
```
SELECT id_kategorie,
       kategorie.nazev,
       count(id_hudebni_nastroj) AS pocet_nastroju,
       sum(cena) as celkova_cena
FROM kategorie
JOIN hudebni_nastroj using(id_kategorie)
WHERE TO_CHAR(datum_zakoupeni, 'YYYY')=2019
GROUP BY id_kategorie,
         kategorie.nazev
HAVING sum(cena) > 200000
ORDER BY celkova_cena DESC;
```


### D21 - Vypiš (id, jmeno, email, tel) všech žáků a těm žákům, k nímž máme evidovaného zákonného zástupce, vypiš (id,jméno, tel a email) zákonného zástupce. Výstup má být seřazen abecedně dle jména žáků.

#### SQL
```
SELECT z.id_zak,
       z.jmeno,
       z.email,
       z.tel,
       zz.id_zakonny_zastupce,
       zz.jmeno AS jmeno_zz,
       zz.tel AS tel_zz,
       zz.email as email_zz
FROM zak z
LEFT JOIN zakonny_zastupce  zz ON zz.id_zakonny_zastupce=z.id_zakonny_zastupce
ORDER BY z.jmeno ASC;
```


### D22 - Seznam všech žáků a jimi půjčených nástrojů, včetně žáků kteří nic půjčeno nemají a nástrojů, které půjčené nejsou. + Ke každému nástroji vypiš kategorii, do které patří.

#### SQL
```
SELECT z.id_zak,
       z.jmeno,
       z.email,
       hn.id_hudebni_nastroj,
       k.nazev AS kategorie,
       hn.nazev AS model
FROM hudebni_nastroj hn 
JOIN kategorie k using(id_kategorie)
LEFT JOIN pujcka p ON hn.id_hudebni_nastroj=p.id_hudebni_nastroj
FULL JOIN zak z ON z.id_zak=p.id_zak;
```



### D23 - Všem učitelům(=zaměstnanům pracujicím na pozici učitel) zvyš plat o 1000kč.

#### SQL
```
--KOMENTOVANE DOTAZY PRO KONTROLU
/*
SELECT id_zamestnanec, plat
FROM zamestnanec
JOIN zamestnanec_pozice_rel using(id_zamestnanec)
JOIN pozice p using(id_pozice) 
WHERE p.nazev='učitel'
ORDER BY id_zamestnanec;
*/

UPDATE zamestnanec SET plat=plat+1000
WHERE id_zamestnanec in 
(
    SELECT id_zamestnanec
    FROM zamestnanec
    JOIN zamestnanec_pozice_rel using(id_zamestnanec)
    JOIN pozice p using(id_pozice) 
    WHERE p.nazev='učitel'
);

/*
SELECT id_zamestnanec, plat
FROM zamestnanec
JOIN zamestnanec_pozice_rel using(id_zamestnanec)
JOIN pozice p using(id_pozice) 
WHERE p.nazev='učitel'
ORDER BY id_zamestnanec;
*/

rollback;
```

### D24 - Smaž všechny půjčky na hudební nástroje z kategorie 'Violoncello'

#### SQL
```
--KOMENTOVANE DOTAZY PRO KONTROLU
/*
SELECT  id_zak,
        z.jmeno,
        id_hudebni_nastroj,
        hn.nazev,
        k.nazev AS kategorie
FROM zak z
JOIN pujcka p using(id_zak)
JOIN hudebni_nastroj hn using(id_hudebni_nastroj)
JOIN kategorie k using(id_kategorie)
WHERE k.nazev='Violoncello';
*/

DELETE FROM pujcka
WHERE id_hudebni_nastroj IN
(
    SELECT id_hudebni_nastroj
    FROM pujcka
    JOIN hudebni_nastroj using(id_hudebni_nastroj)
    JOIN kategorie k using(id_kategorie)
    WHERE k.nazev='Violoncello'
);

/*
SELECT  id_zak,
        z.jmeno,
        id_hudebni_nastroj,
        hn.nazev,
        k.nazev AS kategorie
FROM zak z
JOIN pujcka p using(id_zak)
JOIN hudebni_nastroj hn using(id_hudebni_nastroj)
JOIN kategorie k using(id_kategorie)
WHERE k.nazev='Violoncello';
*/

rollback;
```



### D25 - Zapiš všechny učitele, kteří učí předmět 'Hra na klavír' na pozici 'Korepetitor'. (pouze ty, kteří na dané pozici zapsaní nejsou)

#### SQL
```
--KONTROLNI dotaz -> vypis zamestnance, kteri uci predmet 'Hra na klavir', ale nepracuji na pozici 'korepetitor'
/*
(SELECT DISTINCT id_zamestnanec
FROM zamestnanec z
JOIN zamestnanec_pozice_rel using(id_zamestnanec)
JOIN pozice po using(id_pozice)
JOIN lekce using(id_zamestnanec)
JOIN predmet pr using(id_predmet)
WHERE pr.nazev ='Hra na klavír' AND po.nazev='učitel')

MINUS

(SELECT DISTINCT id_zamestnanec
FROM zamestnanec
JOIN zamestnanec_pozice_rel using(id_zamestnanec)
JOIN pozice using(id_pozice)
WHERE nazev='korepetitor');
*/

INSERT INTO zamestnanec_pozice_rel(id_pozice,id_zamestnanec)
    SELECT  (
                SELECT id_pozice
                FROM pozice
                WHERE nazev='korepetitor'
            ),
            id_zamestnanec
    FROM
    (
        (SELECT DISTINCT id_zamestnanec
        FROM zamestnanec z
        JOIN zamestnanec_pozice_rel using(id_zamestnanec)
        JOIN pozice po using(id_pozice)
        JOIN lekce using(id_zamestnanec)
        JOIN predmet pr using(id_predmet)
        WHERE pr.nazev ='Hra na klavír' AND po.nazev='učitel')
        
        MINUS
        
        (SELECT DISTINCT id_zamestnanec
        FROM zamestnanec
        JOIN zamestnanec_pozice_rel using(id_zamestnanec)
        JOIN pozice using(id_pozice)
        WHERE nazev='korepetitor')
    );
    
--KONTROLNI dotaz => musi byt prazdna mnozina
/*
(SELECT DISTINCT id_zamestnanec
FROM zamestnanec z
JOIN zamestnanec_pozice_rel using(id_zamestnanec)
JOIN pozice po using(id_pozice)
JOIN lekce using(id_zamestnanec)
JOIN predmet pr using(id_predmet)
WHERE pr.nazev ='Hra na klavír' AND po.nazev='učitel')

MINUS

(SELECT DISTINCT id_zamestnanec
FROM zamestnanec
JOIN zamestnanec_pozice_rel using(id_zamestnanec)
JOIN pozice using(id_pozice)
WHERE nazev='korepetitor');
*/
        
ROLLBACK;
```



### D26 - Vytvoř pohled, který pro každý rok půjčky vypíše počet (nově) půjčených hudebnich nástrojů, rozlišených podle kategorie. Následně vypiš kolik bylo v kterém roce půjčeno nástrojů z kategorie 'Housle'.

#### SQL
```
--Vytvor pohled
CREATE VIEW pocet_pujcka_rok AS
SELECT TO_CHAR(p.datum, 'YYYY') as rok,
       k.nazev as kategorie,
       count(id_hudebni_nastroj) as pocet
FROM kategorie k
JOIN hudebni_nastroj using(id_kategorie)
JOIN pujcka p using(id_hudebni_nastroj)
GROUP BY TO_CHAR(p.datum, 'YYYY'),
         k.nazev;

--Proved dotaz
SELECT rok,
       pocet AS pocet_pujcenych_housli
FROM pocet_pujcka_rok
WHERE kategorie='Housle'
ORDER BY rok;

--Smaz pohled
DROP VIEW pocet_pujcka_rok;

```


### D27 - Počet předmětů v jednotlivých oborech

#### SQL
```
SELECT o.id_obor,
       o.nazev,
       count(id_predmet) AS pocet_predmetu
FROM obor o
LEFT JOIN predmet p on o.id_obor=p.id_obor
GROUP BY o.id_obor, o.nazev
ORDER BY pocet_predmetu desc
```

### D28 - Vytvoř pohled všech učitelů a ke každému počet jejích žáků. Následně vypiš počet žáků jednotlivých učitelů, seřazených sestupně dle počtu žáků.

#### SQL
```
--Vytvor pohled
CREATE VIEW ucitele_zaci AS
SELECT id_zamestnanec,
       zam.jmeno,
       count(distinct id_zak) AS pocet_zaku
FROM zamestnanec zam
JOIN zamestnanec_pozice_rel using(id_zamestnanec)
JOIN pozice po using(id_pozice)
JOIN lekce using(id_zamestnanec)
JOIN zak_lekce_rel using(id_lekce)
JOIN zak using(id_zak)
WHERE po.nazev='učitel'
GROUP BY id_zamestnanec,
         zam.jmeno;
 
--Vypis pocet zaku jednotlivych ucitelu, serazenych dle poctu zaku sestupne
SELECT *
FROM ucitele_zaci
ORDER BY pocet_zaku DESC;

--Smaz pohled
DROP VIEW ucitele_zaci;
```



